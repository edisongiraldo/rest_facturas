package rest_facturas

import grails.util.Environment

class BootStrap {

    def init = { servletContext ->
        if(Environment.current == Environment.DEVELOPMENT) {

        	//Clientes
			def fCliente = new Cliente(	
								cedula: "10130756",
								nombre: "Wilson Cortés"
        					)
        					.save()

        	def fFactura = new Factura(
        						numero: "FACTURA 1",
        						cliente: fCliente,
        						fechaFactura: Date.parse( 'yyyy/MM/dd', '2017/05/21')
        					)
        					.addToDetallesFactura(new DetalleFactura(item: "Pan de 2000", cantidad: "5", valor:"2000"))
        					.addToDetallesFactura(new DetalleFactura(item: "Bolsa de leche", cantidad: "2", valor:"2500"))
        					.addToDetallesFactura(new DetalleFactura(item: "Huevos", cantidad: "10", valor:"350"))
        					.save()

			fCliente = new Cliente(	
								cedula: "80850811",
								nombre: "Edison Ordoñez Giraldo"
        					)
        					.save()

        	fFactura = new Factura(
        						numero: "FACTURA 2",
        						cliente: fCliente,
        						fechaFactura: Date.parse( 'yyyy/MM/dd', '2017/05/22')
        					)
        					.addToDetallesFactura(new DetalleFactura(item: "Papa / kg", cantidad: "2", valor:"940"))
        					.addToDetallesFactura(new DetalleFactura(item: "Platano hartón", cantidad: "5", valor:"1000"))
        					.addToDetallesFactura(new DetalleFactura(item: "Yuca / kg", cantidad: "2", valor:"350"))
        					.addToDetallesFactura(new DetalleFactura(item: "Tomate / kg", cantidad: "1", valor:"1100"))
        					.save()

			fCliente = new Cliente(	
								cedula: "6239798",
								nombre: "Christopher J Jackson"
        					)
        					.save()

        	fFactura = new Factura(
        						numero: "FACTURA 3",
        						cliente: fCliente,
        						fechaFactura: Date.parse( 'yyyy/MM/dd', '2017/05/19')
        					)
        					.addToDetallesFactura(new DetalleFactura(item: "Chatas / kg", cantidad: "2", valor:"15000"))
        					.addToDetallesFactura(new DetalleFactura(item: "Muchacho / kg", cantidad: "1", valor:"19900"))
        					.save()

        }
    }
    def destroy = {
    }
}
