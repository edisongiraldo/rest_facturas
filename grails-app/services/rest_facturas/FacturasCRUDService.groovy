package rest_facturas

import grails.transaction.Transactional

@Transactional
class FacturasCRUDService {

    def serviceMethod() {

    }

    def listFactura(){
    	def factura = Factura.list()
    	return factura
    }

    def getFactura(datos){
    	def factura = Factura.get(datos.id)
    	return factura
    }

    def guardarFactura(datos){
    	def factura
    	if (datos.id != null && datos.id != ""){
    		factura = Factura.get(datos.id)
    	}else{
    		factura = new Factura()
    	}

    	//Verifica datos del cliente
    	def cliente
        if (datos.cedula != null){
        	def criteriaCliente = Cliente.createCriteria();
        	def resultCliente = criteriaCliente.list{
        		eq("cedula", datos.cedula)
        	}
        	if (resultCliente != null && resultCliente.size() > 0){
        		cliente = resultCliente.first()
        	}else{
        		cliente = new Cliente(cedula: datos.cedula, nombre: datos.nombre)
        	}
        }

    	//Datos de la factura
        if (datos.numero != null) factura.numero = datos.numero
    	if (datos.fechaFactura != null) factura.fechaFactura = Date.parse( 'yyyy-MM-dd', datos.fechaFactura)
    	if (cliente != null) factura.cliente = cliente

    	factura.save(flush: true, failOnError: true)

    	return factura
    }

    def borrarFactura(datos){
    	def factura = Factura.get(datos.id)
    	factura.delete(flush: true, failOnError: true)
    }

    def listDetalleFactura(datos){
    	def factura = Factura.get(datos.id_factura)
    	return factura.detallesFactura
    }

    def getDetalleFactura(datos){
    	def detalleFactura = DetalleFactura.get(datos.id)
    	return detalleFactura
    }

    def guardarDetalleFactura(datos){

        def detalleFactura
        if (datos.id != null && datos.id != ""){
            detalleFactura = DetalleFactura.get(datos.id)
        }else{
            detalleFactura = new DetalleFactura()
        }

        //Verifica la factura donde se registra el detalle
        def factura = Factura.get(datos.id_factura)


        //Datos del detalle de la factura
        if (datos.item!=null) detalleFactura.item = datos.item
        if(datos.cantidad!=null) detalleFactura.cantidad = Integer.parseInt(datos.cantidad)
        if(datos.valor!=null) detalleFactura.valor = Integer.parseInt(datos.valor)
        detalleFactura.factura = factura

        detalleFactura.save(flush: true, failOnError: true)

        return detalleFactura
    }

    def borrarDetalleFactura(datos){
    	def detallefactura = DetalleFactura.get(datos.id)
    	detallefactura.delete(flush: true, failOnError: true)
    }
}
