package rest_facturas

import grails.transaction.*
import static org.springframework.http.HttpStatus.*
import static org.springframework.http.HttpMethod.*
import grails.converters.*

@Transactional(readOnly = true)
class FacturasRESTController {

	def FacturasCRUDService

    //Action para listar datos de las Facturas
    def index() { 
    	def list = FacturasCRUDService.listFactura()

    	def facturaREST = []
	    def listFactura = []

	    list.each {

	    		facturaREST = [
                    id: it.id,
					numero : it.numero,
		    		fechaFactura : it.fechaFactura,
		    		nombreCliente : it.cliente.nombre,
		    		nitCliente : it.cliente.cedula,
                    totalFactura: it.totalFactura
	    		]
	    		listFactura << facturaREST

		}

    	render listFactura as JSON
    }

    //Action para listar datos de una sola factura
    def getFactura(){

        def factura = FacturasCRUDService.getFactura(params)

        def facturaREST = [
            id: factura.id,
            numero : factura.numero,
            fechaFactura : factura.fechaFactura,
            nombreCliente : factura.cliente.nombre,
            nitCliente : factura.cliente.cedula,
            totalFactura: factura.totalFactura
        ]

    	render facturaREST as JSON
    }

    def listDetalleFactura(){

        def list = FacturasCRUDService.listDetalleFactura(params);

        def detalleFacturaREST = []
        def listDetalleFactura = []

        list.each {

                detalleFacturaREST = [
                    id: it.id,
                    cantidad : it.cantidad,
                    id_factura : it.factura.id,
                    item : it.item,
                    valor: it.valor,
                    totalItem : (it.cantidad * it.valor)
                ]
                listDetalleFactura << detalleFacturaREST

        }
        render listDetalleFactura as JSON
    }

    def getDetalleFactura(){

        def detalle = FacturasCRUDService.getDetalleFactura(params)

        def detalleFacturaREST = [
            id: detalle.id,
            cantidad : detalle.cantidad,
            id_factura : detalle.factura.id,
            item : detalle.item,
            valor: detalle.valor,
            totalItem : (detalle.cantidad * detalle.valor)
        ]

    	render detalleFacturaREST as JSON
    }

    def guardaFactura(){
    	render FacturasCRUDService.guardarFactura(params) as JSON
    }

    def borrarFactura(){
        FacturasCRUDService.borrarFactura(params)
        render {borrado: true} as JSON
    }

    def guardaDetalleFactura(){
        render FacturasCRUDService.guardarDetalleFactura(params) as JSON
    }
    
    def borrarDetalleFactura(){
        FacturasCRUDService.borrarDetalleFactura(params)
        render {borrado: true} as JSON
    }
}
