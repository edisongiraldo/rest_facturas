package rest_facturas

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/index")
        "500"(view:'/error')
        "404"(view:'/notFound')

    "/facturasR"{
            controller = "FacturasREST"
            action = [
                GET: "index",
                POST: "guardaFactura"
            ]
        }    

    "/facturasR/$id"{
            controller = "FacturasREST"
            action = [
                GET: "getFactura",
                PATCH: "guardaFactura",
                DELETE: "borrarFactura"
            ]
        }    


    "/facturasR/$id_factura/detalle"{
            controller = "FacturasREST"
            action = [
                GET: "listDetalleFactura",
                POST: "guardaDetalleFactura"
            ]
        } 

    "/facturasR/$id_factura/detalle/$id"{
            controller = "FacturasREST"
            action = [
                GET: "getDetalleFactura",
                PATCH: "guardaDetalleFactura",
                DELETE: "borrarDetalleFactura"
            ]
        } 

    }

}
