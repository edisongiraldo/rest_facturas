package rest_facturas

class Factura {

	String	numero
	Date 	fechaFactura
	Cliente	cliente
    Date   	dateCreated
    Date   	lastUpdated	

    static transients = ['nombreCliente', 'totalFactura']

    String getNombreCliente() {
    	cliente.nombre
    }

    Long getTotalFactura() {
    	detallesFactura.sum { it.cantidad * it.valor }
    }

    static hasMany = [detallesFactura: DetalleFactura]

    static constraints = {
    	numero nullable: false, blank: false, unique: true
    }
}
