package rest_facturas

class Cliente {

	String 	nombre
	String	cedula
    Date   	dateCreated
    Date   	lastUpdated	

    static constraints = {
    	nombre	nullable: false, blank: false
    	cedula	nullable: false, blank: false, unique: true
    }
}
