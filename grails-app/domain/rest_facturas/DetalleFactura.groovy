package rest_facturas

class DetalleFactura {

    String  item
    int     cantidad
    int     valor
    Date    dateCreated
    Date    lastUpdated 

    static belongsTo = [factura: Factura]

    static constraints = {
    	item nullable: false, blank: false
    }
}
